#
#  Tobias Gröger, Karlsruhe. 2022
#
#  clang-format-14 file for C++ projects
#

---
Language: Cpp
BasedOnStyle: LLVM

ColumnLimit: 100

# Indentation
UseTab: Never
IndentWidth: 4
ContinuationIndentWidth: 2
ConstructorInitializerIndentWidth: 2
IndentAccessModifiers: false
AccessModifierOffset: -2

NamespaceIndentation: All
IndentCaseLabels: true
IndentPPDirectives: BeforeHash


# Line breaks and alignment
BreakBeforeBraces: Linux

BreakBeforeBinaryOperators: NonAssignment
BreakBeforeTernaryOperators: true

AlignAfterOpenBracket: Align
AlignOperands: AlignAfterOperator
AlignTrailingComments: true
AlignConsecutiveMacros: AcrossEmptyLines

BinPackArguments: false
AllowAllArgumentsOnNextLine: true
BinPackParameters: false
AllowAllParametersOfDeclarationOnNextLine: true

PackConstructorInitializers: NextLine
BreakConstructorInitializers: AfterColon
BreakInheritanceList: AfterColon

AlwaysBreakTemplateDeclarations: Yes
BreakBeforeConceptDeclarations: true

AllowShortBlocksOnASingleLine: Empty
AllowShortCaseLabelsOnASingleLine: true
AllowShortEnumsOnASingleLine: true
AllowShortFunctionsOnASingleLine: Empty
AllowShortIfStatementsOnASingleLine: WithoutElse
AllowShortLambdasOnASingleLine: All
AllowShortLoopsOnASingleLine: true

AlwaysBreakAfterReturnType: AllDefinitions

EmptyLineBeforeAccessModifier: LogicalBlock

AlwaysBreakBeforeMultilineStrings: false
BreakStringLiterals: false

MaxEmptyLinesToKeep: 2
ReflowComments: false


# Spaces
Cpp11BracedListStyle: false
SpaceBeforeCpp11BracedList: true

PointerAlignment: Left

SpaceAfterCStyleCast: true


# Other stuff (comments, auto derives etc.)
DeriveLineEnding: true
DerivePointerAlignment: false

FixNamespaceComments: true


# Includes
SortIncludes: true
IncludeBlocks: Regroup

IncludeCategories:
# Pico Libraries
  - Regex: '^<(pico|hardware)/'
    Priority: 50
    SortPriority: 50

# LibPico++ Library
  - Regex: "^<picopp/"
    Priority: 50
    SortPriority: 51

# Header in <> without extension (C++ STL header)
  - Regex: '^<[^.]*>'
    Priority: 10
    SortPriority: 11
# Header in <> with extension (C header/other libraries)
  - Regex: '^<[^.]*\.[^.]*>'
    Priority: 10
    SortPriority: 10

# Local Header in ""
  - Regex: '^"'
    Priority: 2
