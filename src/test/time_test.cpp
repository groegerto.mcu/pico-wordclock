#include <gtest/gtest.h>
#include <wordclock/led/WordController.h>
#include <wordclock/led/WordDefinitions.h>
#include <chrono>
#include <tuple>
#include <unordered_set>

using namespace std::chrono;
using WallTime = std::chrono::system_clock::time_point;
using namespace std::chrono_literals;
using Indices = std::unordered_set<uint8_t>;

class TimeTestFixture : public ::testing::TestWithParam<std::tuple<WallTime, Indices>>
{
  protected:
    void
    SetUp() override
    {
        time = std::get<0>(GetParam());
        expected = std::get<1>(GetParam());
    }

    int8_t
    hour(std::optional<WallTime> t = std::nullopt)
    {
        const std::time_t tt = system_clock::to_time_t(t.value_or(time));
        return std::localtime(&tt)->tm_hour;
    }
    int8_t
    min(std::optional<WallTime> t = std::nullopt)
    {
        const std::time_t tt = system_clock::to_time_t(t.value_or(time));
        return std::localtime(&tt)->tm_min;
    }

    Indices indices;
    WallTime time;
    Indices expected;
};

static uint8_t oClock[] = { UHR };
static uint8_t itIs[] = { ES, IST };

static WallTime
createTime(int8_t hour, int8_t min)
{
    std::tm utc_tm = { 0 };
    utc_tm.tm_hour = hour;
    utc_tm.tm_min = min;

    std::time_t t = std::mktime(&utc_tm);

    return system_clock::from_time_t(t);
}

template <typename T>
static void
set(std::unordered_set<T>& set, const std::vector<T>& vec)
{
    set.clear();
    for (const T& i : vec) {
        set.insert(i);
    }
}

TEST_P(TimeTestFixture, TestPlainTime)
{
    set(indices,
        wordclock::led::WordController::getTimeWords(hour(), min(), 0, wordclock::NEVER, false));

    EXPECT_EQ(expected, indices) << "Plain time (" << std::to_string(hour()) << ":"
                                 << std::to_string(min()) << ") must match words";
}

TEST_P(TimeTestFixture, TestItIsTime)
{
    Indices expectedItIs = expected;
    for (const auto& i : itIs) {
        expectedItIs.insert(i);
    }
    set(indices,
        wordclock::led::WordController::getTimeWords(hour(), min(), 0, wordclock::ALWAYS, false));

    EXPECT_EQ(expectedItIs, indices) << "Time must include it is for every time";

    set(
      indices,
      wordclock::led::WordController::getTimeWords(hour(), min(), 0, wordclock::FULL_HALF, false));

    if (min() == 0 || min() == 30) {
        EXPECT_EQ(expectedItIs, indices) << "Time must include it is for full and half hours";
    } else {
        EXPECT_EQ(expected, indices) << "Time must not include it is for in between minutes";
    }
}

TEST_P(TimeTestFixture, TestOClockTime)
{
    Indices expectedOClock = expected;
    for (const auto& i : oClock) {
        expectedOClock.insert(i);
    }
    set(indices,
        wordclock::led::WordController::getTimeWords(hour(), min(), 0, wordclock::NEVER, true));

    if (min() == 0) {
        EXPECT_EQ(expectedOClock, indices) << "Time must include o clock for full hours";
    } else {
        EXPECT_EQ(expected, indices) << "Time must not include o clock for non full hours";
    }
}

TEST_P(TimeTestFixture, TestOffsetTime)
{
    WallTime offsetTime;
    for (int8_t offset = 0; offset <= 4; offset++) {
        offsetTime = time - offset * 1min;
        set(indices,
            wordclock::led::WordController::getTimeWords(
              hour(offsetTime), min(offsetTime), offset, wordclock::NEVER, false));
        EXPECT_EQ(expected, indices)
          << "Time (" << std::to_string(hour(offsetTime)) << ":" << std::to_string(min(offsetTime))
          << ") must display as (" << std::to_string(hour()) << ":" << std::to_string(min())
          << ") with offset: " << std::to_string(offset);

        offsetTime = time + (4 - offset) * 1min;
        set(indices,
            wordclock::led::WordController::getTimeWords(
              hour(offsetTime), min(offsetTime), offset, wordclock::NEVER, false));
        EXPECT_EQ(expected, indices)
          << "Time (" << std::to_string(hour(offsetTime)) << ":" << std::to_string(min(offsetTime))
          << ") must display as (" << std::to_string(hour()) << ":" << std::to_string(min())
          << ") with offset: " << std::to_string(offset);
    }
}


INSTANTIATE_TEST_SUITE_P(
  TimeTests,
  TimeTestFixture,
  ::testing::Values(std::make_tuple(createTime(0, 0), Indices { ZWOELF }),
                    std::make_tuple(createTime(1, 0), Indices { EIN }),
                    std::make_tuple(createTime(2, 0), Indices { ZWEI }),
                    std::make_tuple(createTime(3, 0), Indices { DREI }),
                    std::make_tuple(createTime(4, 0), Indices { VIER }),
                    std::make_tuple(createTime(5, 0), Indices { FUENF }),
                    std::make_tuple(createTime(6, 0), Indices { SECHS }),
                    std::make_tuple(createTime(7, 0), Indices { SIEBEN }),
                    std::make_tuple(createTime(8, 0), Indices { ACHT }),
                    std::make_tuple(createTime(9, 0), Indices { NEUN }),
                    std::make_tuple(createTime(10, 0), Indices { ZEHN }),
                    std::make_tuple(createTime(11, 0), Indices { ELF }),
                    std::make_tuple(createTime(12, 0), Indices { ZWOELF }),
                    std::make_tuple(createTime(10, 5), Indices { ZEHN, FUENF2, NACH }),
                    std::make_tuple(createTime(10, 10), Indices { ZEHN, ZEHN2, NACH }),
                    std::make_tuple(createTime(10, 15), Indices { ZEHN, VIERTEL, NACH }),
                    std::make_tuple(createTime(10, 20), Indices { ZEHN, ZWANZIG, NACH }),
                    std::make_tuple(createTime(10, 25), Indices { ELF, FUENF2, VOR, HALB }),
                    std::make_tuple(createTime(10, 30), Indices { ELF, HALB }),
                    std::make_tuple(createTime(10, 35), Indices { ELF, FUENF2, NACH, HALB }),
                    std::make_tuple(createTime(10, 40), Indices { ELF, ZWANZIG, VOR }),
                    std::make_tuple(createTime(10, 45), Indices { ELF, VIERTEL, VOR }),
                    std::make_tuple(createTime(10, 50), Indices { ELF, ZEHN2, VOR }),
                    std::make_tuple(createTime(10, 55), Indices { ELF, FUENF2, VOR })));
