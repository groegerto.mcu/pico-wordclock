# Wordclock Firmware

This is the firmware to run on the Wordclock.

[[_TOC_]]

## Dependencies

- [Pico C SDK](https://github.com/raspberrypi/pico-sdk)
- [PicoLib++](https://gitlab.com/groegerto.mcu/lib-picopp)

To build this project, first follow the instructions under these two repositories.

Additionally there are dependencies on ArduinoJson and gcem as git submodules which can be pulled with `git submodule update --init`.

## Building and Testing

To build this project execute following commands in the `src/` directory. This will build a `wordclock.uf2` file under `src/build/` which can be loaded onto the Pico W.
```bash
mkdir build && cd build
cmake .. && make
```

There is a unit test to check the proper calculation of which leds to illuminate for a given time and configuration.
This test is a separate cmake project meant to run on the host machine. To build run the following in the `src/` directory:
```bash
cd test/
mkdir build && cd build
cmake .. && make
./wordclock_test
```

### Server file system

The http server running on the Pico uses a compiled in file system.
The files accessible are located under `src/server-fs/` and will be compiled to `src/http_config/wordclock_fsdata.c`.
A version will be supplied through this repository but to update this file, call `src/generate-server-file-system.sh`.


## Functionality

The clock saves its configuration to flash so it is persisted through power cycles.

When powering on and no wifi is configured or the currently configured wifi is not available it will setup an access point available at `wordclock`.

The configuration page is accessible at `wordclock.local` when your device supports mDNS requests with a built in mDNS responder in the clock.
Otherwise when the router properly sets up DNS entries with the host name provided through DHCP the page is also accessible through `wordclock/` directly.
This does not work for the access point setup via the clock itself.


### Configuration

After every configuration change the configuration has to be saved to the Pico with the `Speichern` button at the bottom of the page.

Some global options for the clock can be set like the ntp server or the interval when the time should be updated.
Additionally the time zone can be configured and whether to automatically adjust for daylight saving time.
This is the daylight saving time in the European Union.
As the clock only displays the time in five minute steps the time when the next step should be displayed can be adjusted with the `Uhrzeit Offset`.
A value of 0 means the time is always rounded down, so 10:49 is displayed as 10:45.
A value of 1 means the time is always displayed one minute early, so 10:48 is displayed as 10:45 but 10:49 is displayed as 10:50.
Only values between 0 and 4 are meaningful.

<img src="../doc/Images/config1.png" alt="front plate" width="50%"/>

There is also a timer functionality which automatically turns the clock off for certain times.
It is trigger based so the clock is turned on/off one time exactly when the trigger happens.
In between it can be turned on/off with the buttons below the timer configuration.
This will be overwritten the next time a timer triggers.

<img src="../doc/Images/config2.png" alt="front plate" width="50%"/>

You can also set which wifi to connect to.
This is the most important setting as otherwise the clock has no time information.
A connection is attempted at power on of the clock, if the connection couldn't be established the clock sets up its own access point and doesn't try any more automatic connection attempts.

<img src="../doc/Images/config3.png" alt="front plate" width="50%"/>


## API

The clock has the following pages:

- `/index.shtml` the main configuration page
- `/watchdog-info.shtml` a page reporting whether the clock crashed since the last power cycle
- `/favicon.png`, `/favicon.svg` the favicon of the webpage

Additionally there are the following `REST` API calls:

- GET `/reconnect`, attempt a connection to the currently configured wifi network
- GET `/reboot`, reboot the Pico
- GET `/override?dir=<-1,0,1>`, set override.
This is equivalent to the enable/disable buttons below the timer configuration.
0 means disable override, use value from timer.
1 means override high, enable display until next timer.
-1 means override low, disable display until next timer.
- GET `/config.json`, get the current configuration as json
- POST `/save-configuration.post`, save a new configuration.
This will override all provided values in the current configuration and save them to flash.
