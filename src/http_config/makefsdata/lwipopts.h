#ifndef _LWIPOPTS_LIB_PICOPP
#define _LWIPOPTS_LIB_PICOPP

//-----------------------------------------------
// This file is only used for building the makefsdata command line tool!
//-----------------------------------------------


#define HTTPD_ADDITIONAL_CONTENT_TYPES {"svg", HTTP_HDR_SVG}

#endif /* _LWIPOPTS_LIB_PICOPP */
