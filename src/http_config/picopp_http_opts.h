#pragma once


#define HTTPD_FSDATA_FILE          <wordclock_fsdata.c>

#define LWIP_HTTPD_CGI             1

#define LWIP_HTTPD_SSI             1

#define LWIP_HTTPD_SSI_INCLUDE_TAG 1

#define LWIP_HTTPD_CUSTOM_FILES    1

#define LWIP_HTTPD_SUPPORT_POST    1
