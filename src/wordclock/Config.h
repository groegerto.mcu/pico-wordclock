#pragma once

#include <ArduinoJson.hpp>
#include <wordclock/led/WordController.h>
#include <wordclock/time/Timer.h>

#include <picopp/hardware/String.hpp>
#include <picopp/led/Color.h>
#include <picopp/time/Duration.h>

namespace wordclock
{
    namespace conf
    {
        static constexpr picopp::Duration MAIN_LOOP_DURATION = picopp::Duration::ms(10);
        static constexpr picopp::Duration WATCHDOG_TIMEOUT = picopp::Duration::s(5);

        static constexpr const char* AP_SSID = "wordclock";
        static constexpr const char* AP_PASSWD = "wordclock";

        static constexpr picopp::Duration NTP_RETRY_TIME = picopp::Duration::s(20);

        static constexpr uint BRIGHTNESS_SENSOR_PIN = 26;
        static constexpr uint BRIGHTNESS_SENSOR_MAX_BRIGHTNESS = 1'100;
        static constexpr uint BRIGHTNESS_SENSOR_MIN_BRIGHTNESS = 10;
        static constexpr float BRIGHTNESS_MAX_OUTPUT = 0.784; // fractional output at max brightness
        static constexpr picopp::Duration BRIGHTNESS_ANIMATION_DURATION = picopp::Duration::ms(800);
        static constexpr uint8_t BRIGHTNESS_HYSTERESIS = 16;

        static constexpr uint LED_PIN = 12;

        static constexpr uint NUM_TIMER_SLOTS = 8;

        extern bool RESTART_CAUSED_BY_WATCHDOG;

    } // namespace conf


    struct Config {
        bool enabled;

        // network configuration
        picopp::FixedString<64> ssid;
        picopp::FixedString<64> passwd;

        // clock configuration
        picopp::FixedString<128> ntpServer;
        picopp::Duration ntpUpdateInterval;
        int16_t timeZone;
        bool automaticDST;
        int8_t roundOffset;

        // display configuration
        DisplayMode displayMode;
        bool showOClock;
        uint8_t brightness;
        bool automaticBrightness;
        bool showTestPattern;
        uint16_t powerLimit;


        // color configuration
        picopp::CRGBW foregroundColor;
        picopp::CRGBW backgroundColor;

        time::Timer timer[conf::NUM_TIMER_SLOTS];

        std::string
        generateJson() const
        {
            ArduinoJson::StaticJsonDocument<2048> json;

            json["en"] = enabled;

            json["ssid"] = ssid.str();

            json["ntpServ"] = ntpServer.str();
            json["ntpUpdate"] = ntpUpdateInterval.s();
            json["tZ"] = timeZone;
            json["dst"] = automaticDST;
            json["roundO"] = roundOffset;

            json["dspMode"] = (uint8_t) displayMode;
            json["showC"] = showOClock;
            json["bright"] = brightness;
            json["autoB"] = automaticBrightness;
            json["showTest"] = showTestPattern;
            json["powerL"] = powerLimit; // powerLimit in mW

            json["fCol"] = foregroundColor.getColorCode();
            json["bCol"] = backgroundColor.getColorCode();

            ArduinoJson::JsonArray array = json.createNestedArray("dispTimer");
            for (const time::Timer& i : timer) {
                if (i.active) {
                    ArduinoJson::JsonObject t = array.createNestedObject();
                    t["fromDotw"] = i.from.dotw;
                    t["fromHour"] = i.from.hour;
                    t["fromMin"] = i.from.min;
                    t["toDotw"] = i.to.dotw;
                    t["toHour"] = i.to.hour;
                    t["toMin"] = i.to.min;
                }
            }

            std::string serialized;
            serialized.reserve(256);
            ArduinoJson::serializeJson(json, serialized);
            return serialized;
        }

        bool
        fromJson(char* content)
        {
            // will modify the input char* so less memory is needed for json
            ArduinoJson::StaticJsonDocument<2048> json;
            ArduinoJson::DeserializationError error = ArduinoJson::deserializeJson(json, content);

            if (error) {
                printf("[Wordclock] Error! json could not be parsed '%s'\n", error.c_str());
                return true;
            }

            enabled = json["en"] | enabled;

            ssid = json["ssid"] | (const char*) ssid.str();
            passwd = json["passwd"] | (const char*) passwd.str();

            ntpServer = json["ntpServ"] | (const char*) ntpServer.str();
            ArduinoJson::JsonVariant update = json["ntpUpdate"];
            if (!update.isNull()) {
                ntpUpdateInterval = picopp::Duration::s(update);
            }
            timeZone = json["tZ"] | timeZone;
            automaticDST = json["dst"] | automaticDST;
            roundOffset = json["roundO"] | roundOffset;

            displayMode = static_cast<DisplayMode>(json["dspMode"] | (uint8_t) displayMode);
            showOClock = json["showC"] | showOClock;
            brightness = json["bright"] | brightness;
            automaticBrightness = json["autoB"] | automaticBrightness;
            showTestPattern = json["showTest"] | showTestPattern;
            powerLimit = json["powerL"] | powerLimit;

            ArduinoJson::JsonVariant foreground = json["fCol"];
            if (!foreground.isNull()) {
                foregroundColor.setColorCode(foreground.as<uint32_t>());
            }
            ArduinoJson::JsonVariant background = json["bCol"];
            if (!background.isNull()) {
                backgroundColor.setColorCode(background.as<uint32_t>());
            }

            ArduinoJson::JsonArray array = json["dispTimer"];
            if (!array.isNull()) {
                int i = 0;
                for (ArduinoJson::JsonVariant entry : array) {
                    // just ignore any entries if they do not fit into the array
                    if (i >= conf::NUM_TIMER_SLOTS) {
                        break;
                    }
                    timer[i].active = true;
                    timer[i].triggered = false;
                    timer[i].from.dotw = entry["fromDotw"];
                    timer[i].from.hour = entry["fromHour"];
                    timer[i].from.min = entry["fromMin"];
                    timer[i].to.dotw = entry["toDotw"];
                    timer[i].to.hour = entry["toHour"];
                    timer[i].to.min = entry["toMin"];

                    i++;
                }
                for (; i < conf::NUM_TIMER_SLOTS; i++) {
                    timer[i].active = false;
                }
            }

            return false;
        }
    };

} // namespace wordclock
