#include <stdio.h>
#include <wordclock/Config.h>
#include <wordclock/FlashManager.h>
#include <wordclock/led/BrightnessController.h>
#include <wordclock/led/LedController.h>
#include <wordclock/network/NetworkManager.h>
#include <wordclock/network/Webserver.h>
#include <wordclock/time/DisplayTimer.h>
#include <wordclock/time/TimeManager.h>

#include <pico/stdlib.h>
#include <picopp/hardware/Watchdog.h>

using namespace picopp::literals;
using namespace wordclock;

bool conf::RESTART_CAUSED_BY_WATCHDOG = false;

enum Override { NONE, ON, OFF };

int
main()
{
    // initialize stdio
    stdio_init_all();

    conf::RESTART_CAUSED_BY_WATCHDOG = picopp::Watchdog::timerCausedReboot();

    Override currentOverride = NONE;

    const auto overrideHandler = [&currentOverride](int direction) {
        if (direction == 0) {
            currentOverride = NONE;
        } else {
            currentOverride = direction > 0 ? ON : OFF;
        }
    };

    FlashManager flash;
    Config& config = flash.getConfig();
    network::NetworkManager network { config };
    time::TimeManager time { config, network };
    time::DisplayTimer displayTimer { config };
    network::Webserver server { flash, network, time, overrideHandler };
    led::BrightnessController brightness { config };
    led::LedController led { config };

    if (config.showTestPattern) {
        puts("Displaying test pattern...");
        led.showTestPattern();
    }

    // enable the watchdog with a large timer, so reboot is only performed when actually stuck
    picopp::Watchdog::enable(conf::WATCHDOG_TIMEOUT);

    picopp::Timestamp timer = picopp::Timestamp::in(conf::MAIN_LOOP_DURATION);
    while (1) {

        network.run();
        time.run();
        picopp::Datetime displayTime = time.getDisplayTime();

        auto [triggeredTimer, edgeDetected] = displayTimer.timerActive(displayTime);
        if (edgeDetected) {
            currentOverride = NONE;
        }
        bool displayEnabled = config.enabled && !triggeredTimer;
        if (currentOverride != NONE) {
            displayEnabled = config.enabled && currentOverride == ON;
        }
        if (displayEnabled) {
            uint8_t currentBrightness = brightness.run();
            led.run(displayTime, currentBrightness);
        } else {
            led.turnOff();
        }

        picopp::Watchdog::update();
        timer.sleepUntil();
        timer = picopp::Timestamp::in(conf::MAIN_LOOP_DURATION);
    }

    return 0;
}
