#pragma once

#include <wordclock/Config.h>
#include <wordclock/time/Timer.h>

#include <picopp/time/Datetime.h>

namespace wordclock::time
{

    class DisplayTimer
    {
      public:
        DisplayTimer(Config& config);

        std::pair<bool, bool> timerActive(const picopp::Datetime& currentTime);

      private:
        bool handleTimer(bool& timerActive, Timer& timer, const picopp::Datetime& currentTime);
        bool timeStampsMatch(const TimerTimestamp& timer, const picopp::Datetime& time);

      private:
        Config& config;
    };

} // namespace wordclock::time
