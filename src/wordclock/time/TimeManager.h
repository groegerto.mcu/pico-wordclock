#pragma once

#include <wordclock/Config.h>
#include <wordclock/network/NetworkManager.h>

#include <picopp/time/Datetime.h>
#include <picopp/wifi/NtpClient.h>

namespace wordclock::time
{

    class TimeManager
    {
      public:
        TimeManager(const Config& config, const network::NetworkManager& network);

        TimeManager(const TimeManager&) = delete;
        TimeManager& operator=(const TimeManager&) = delete;

        void run();

        picopp::Datetime getDisplayTime() const;

      private:
        void ntpCallback(picopp::NtpClient::Status status, picopp::Datetime time);

      private:
        const Config& config;
        const network::NetworkManager& network;
        picopp::NtpClient ntp;
        picopp::Timestamp ntpUpdate;
    };

} // namespace wordclock::time
