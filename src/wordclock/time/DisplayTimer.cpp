#include "DisplayTimer.h"

namespace wordclock::time
{

    DisplayTimer::DisplayTimer(Config& config) : config(config) {}

    /*! \return a pair of <is timer triggered, was edge detected>   */
    std::pair<bool, bool>
    DisplayTimer::timerActive(const picopp::Datetime& currentTime)
    {
        bool triggeredTimer = false;
        bool edgeDetected = false;
        // run through all timer, to handle their triggered parameters
        for (Timer& t : config.timer) {
            edgeDetected |= handleTimer(triggeredTimer, t, currentTime);
        }
        return { triggeredTimer, edgeDetected };
    }


    bool
    DisplayTimer::handleTimer(bool& timerActive, Timer& timer, const picopp::Datetime& currentTime)
    {
        if (!timer.active) return false;

        if (!timer.triggered) {
            if (timeStampsMatch(timer.from, currentTime)) {
                timer.triggered = true;
                timerActive = true;
                return true;
            } else {
                return false;
            }
        } else {
            if (timeStampsMatch(timer.to, currentTime)) {
                timer.triggered = false;
                return true;
            } else {
                timerActive = true;
                return false;
            }
        }
    }

    bool
    DisplayTimer::timeStampsMatch(const TimerTimestamp& timer, const picopp::Datetime& time)
    {
        return (timer.dotw == -1 || (timer.dotw == -2 && 1 <= time->dotw && time->dotw <= 5)
                || timer.dotw == time->dotw)
            && (timer.hour == time->hour) && (timer.min == time->min);
    }


} // namespace wordclock::time
