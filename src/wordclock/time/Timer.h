#pragma once

#include <inttypes.h>

namespace wordclock::time
{

    struct TimerTimestamp {
        int8_t dotw; ///< 0..6, 0 is Sunday, -1 for every day
        int8_t hour; ///< 0..23
        int8_t min;  ///< 0..59
    };

    struct Timer {
        bool active;
        bool triggered;
        TimerTimestamp from;
        TimerTimestamp to;
    };

} // namespace wordclock::time
