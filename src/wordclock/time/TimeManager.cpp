#include "TimeManager.h"

namespace wordclock::time
{

    using namespace picopp::literals;

    TimeManager::TimeManager(const Config& config, const network::NetworkManager& network) :
      config(config), network(network), ntp(config.ntpServer), ntpUpdate(picopp::Timestamp::now())
    {
        picopp::Datetime::initRTC();

        if (ntp.init()) {
            puts("[Wordclock] Error! Cannot initialize NtpClient");
        }
        ntp.setCallback([this](picopp::NtpClient::Status status, picopp::Datetime time) {
            this->ntpCallback(status, time);
        });
    }

    void
    TimeManager::run()
    {
        ntp.loop();
        if (ntpUpdate.reached()) {
            if (!network.hasInternet()) {
                // no network, no point in sending an ntp request
                return;
            }
            ntpUpdate = picopp::Timestamp::in(config.ntpUpdateInterval);
            ntp.setNtpHostname(config.ntpServer);
            ntp.update(1_m, 5);
        }
    }

    picopp::Datetime
    TimeManager::getDisplayTime() const
    {
        picopp::Datetime time = picopp::Datetime::fromRtc();
        // DST calculation is done in UTC
        if (config.automaticDST && time.isDST()) {
            time->hour++;
        }
        time->hour += config.timeZone;
        time.recalculate(); // update all fields

        return time;
    }


    void
    TimeManager::ntpCallback(picopp::NtpClient::Status status, picopp::Datetime time)
    {
        if (status == picopp::NtpClient::OK) {
            time.setRtc();
            printf("[Wordclock] ntp update received: %s (UTC)\n", time.format().c_str());
        } else {
            printf("[Wordclock] ntp callback failed (status: %d)!\n", status);
            // if result failed, retry again
            ntpUpdate = picopp::Timestamp::in(conf::NTP_RETRY_TIME);
        }
    }

} // namespace wordclock::time
