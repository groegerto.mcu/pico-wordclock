#include "FlashManager.h"

namespace wordclock
{

    FlashManager::FlashManager() : flash(true)
    {
        if (flash->magic_number != MAGIC_NUMBER) {
            flash->config = loadDefault();
            flash->magic_number = MAGIC_NUMBER;
            flash.save();
            puts("[Wordclock] config in flash is invalid, loading default");
        } else {
            puts("[Wordclock] loading config from flash");
        }
    }

    Config&
    FlashManager::getConfig()
    {
        return flash->config;
    }

    void
    FlashManager::storeToFlash()
    {
        flash.save();
    }

    Config
    FlashManager::loadDefault()
    {
        return Config {
            .enabled = true,

            .ssid = "",
            .passwd = "",

            .ntpServer = "pool.ntp.org",
            .ntpUpdateInterval = picopp::Duration::h(1),
            .timeZone = 0,
            .automaticDST = false,
            .roundOffset = 0,

            .displayMode = ALWAYS,
            .showOClock = true,
            .brightness = 255,
            .automaticBrightness = true,
            .showTestPattern = true,
            .powerLimit = 2'000,

            .foregroundColor = picopp::CRGBW(0, 0, 0, 255),
            .backgroundColor = picopp::CRGBW(0, 0, 0, 0),

            .timer = { 0 },
        };
    }

} // namespace wordclock
