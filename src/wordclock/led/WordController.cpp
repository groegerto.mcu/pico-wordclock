#include "WordController.h"

#include <wordclock/led/WordDefinitions.h>

namespace wordclock::led
{

    /*! \brief Insert vector b at the end of vector a. */
    template <typename T>
    static std::vector<T>&
    operator+=(std::vector<T>& a, const std::vector<T>& b)
    {
        a.insert(std::end(a), std::begin(b), std::end(b));
        return a;
    }


    std::vector<uint8_t>
    WordController::getTimeWords(
      int8_t hour, int8_t minute, int8_t roundingOffset, DisplayMode mode, bool showOClock)
    {
        std::vector<uint8_t> indices;

        // offset minute so next time is already displayed earlier
        int8_t offsetMin = minute + roundingOffset;
        int8_t roundedMin = offsetMin / 5;
        if (offsetMin >= 25) {
            // fünf vor halb -> needs next hour
            hour++;
        }
        // adjust minute to the interval [0,11]
        roundedMin %= 12;
        // adjust hour to the inverval [1,12]
        hour = ((hour + 11) % 12) + 1;

        if (mode == ALWAYS || mode == FULL_HALF && (roundedMin == 0 || roundedMin == 6)) {
            indices += definitions::IT_IS;
        }
        if (showOClock && roundedMin == 0) {
            indices += definitions::O_CLOCK;
        }

        if (hour == 1) {
            // if hour is 1: use EIN for full hour, otherwise EINS
            hour = roundedMin == 0 ? 0 : 1;
        }
        indices += definitions::HOURS[hour];
        indices += definitions::MINUTES[roundedMin];

        return indices;
    }

} // namespace wordclock::led
