#include "LedController.h"

namespace wordclock::led
{

    using namespace picopp::literals;

    LedController::LedController(const Config& config) :
      config(config),
      leds(),
      controller(leds, NUM_LEDS, conf::LED_PIN),
      powerManager(controller, config.powerLimit)
    {
        powerManager.show();
    }

    void
    LedController::run(picopp::Datetime time, uint8_t brightness)
    {
        // no changes since last update (every change occurs with a minute change), do nothing
        if (time->min == currentMin && brightness == currentBrightness) return;

        currentMin = time->min;
        currentBrightness = brightness;

        controller.setBrightness(brightness);

        applyTime(time);
        powerManager.setPowerLimit(config.powerLimit);
        uint8_t actualBrightness = powerManager.show();
        static bool shown = false;
        if (brightness != actualBrightness) {
            if (!shown)
                printf("[Wordclock] Power limit reached, brightness dimmed to %d\n",
                       actualBrightness);
            shown = true;
        } else {
            shown = false;
        }
    }

    void
    LedController::turnOff()
    {
        // already off, do nothing
        if (currentBrightness == 0) return;

        currentBrightness = 0;

        resetLeds({ 0, 0, 0, 0 });
        powerManager.show();
    }

    void
    LedController::showTestPattern()
    {
        controller.setBrightness(255);
        const picopp::Duration wait = 10_ms;
        const auto run = [this, &wait](picopp::CRGBW color) {
            leds[0] = color;
            powerManager.show();
            wait.sleep();
            for (int i = 1; i < NUM_LEDS; i++) {
                leds[i - 1] = { 0, 0, 0, 0 };
                leds[i] = color;
                powerManager.show();
                wait.sleep();
            }
            leds[NUM_LEDS - 1] = { 0, 0, 0, 0 };
            powerManager.show();
        };

        resetLeds({ 0, 0, 0, 0 });
        run(picopp::CRGBW { 128, 0, 0, 0 });
        (1_s).sleep();
        run(picopp::CRGBW { 0, 128, 0, 0 });
        (1_s).sleep();
        run(picopp::CRGBW { 0, 0, 128, 0 });
        (1_s).sleep();
        run(picopp::CRGBW { 0, 0, 0, 128 });
        (1_s).sleep();
        run(picopp::CRGBW { 128, 128, 128, 128 });
        (1_s).sleep();
    }

    void
    LedController::applyTime(picopp::Datetime time)
    {
        resetLeds(config.backgroundColor);
        const auto& leds = WordController::getTimeWords(
          time->hour, time->min, config.roundOffset, config.displayMode, config.showOClock);
        applyLeds(leds, config.foregroundColor);
    }

    void
    LedController::resetLeds(picopp::CRGBW color)
    {
        for (int i = 0; i < NUM_LEDS; i++) {
            leds[i] = color;
        }
    }

    void
    LedController::applyLeds(const std::vector<uint8_t>& indices, const picopp::CRGBW& color)
    {
        for (uint8_t index : indices) {
            leds[index] = color;
        }
    }


} // namespace wordclock::led
