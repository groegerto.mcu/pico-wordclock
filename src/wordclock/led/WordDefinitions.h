#pragma once

#define ES      0, 1
#define IST     3, 4, 5
#define UHR     101, 100, 99

#define VOR     43, 42, 41
#define NACH    36, 35, 34, 33
#define HALB    44, 45, 46, 47
#define VIERTEL 26, 27, 28, 29, 30, 31, 32
#define ZWANZIG 17, 16, 15, 14, 13, 12, 11
#define FUENF2  7, 8, 9, 10
#define ZEHN2   21, 20, 19, 18

#define EINS    65, 64, 63, 62
#define EIN     65, 64, 63
#define ZWEI    58, 57, 56, 55
#define DREI    66, 67, 68, 69
#define VIER    73, 74, 75, 76
#define FUENF   51, 52, 53, 54
#define SECHS   87, 86, 85, 84, 83,
#define SIEBEN  88, 89, 90, 91, 92, 93
#define ACHT    80, 79, 78, 77
#define NEUN    106, 105, 104, 103
#define ZEHN    109, 108, 107, 106
#define ELF     49, 50, 51
#define ZWOELF  94, 95, 96, 97, 98

#include <inttypes.h>
#include <vector>

namespace wordclock::led::definitions
{

    const std::vector<uint8_t> IT_IS { ES, IST };
    const std::vector<uint8_t> O_CLOCK { UHR };
    const std::vector<uint8_t> MINUTES[12] { {},
                                             { FUENF2, NACH },
                                             { ZEHN2, NACH },
                                             { VIERTEL, NACH },
                                             { ZWANZIG, NACH },
                                             { FUENF2, VOR, HALB },
                                             { HALB },
                                             { FUENF2, NACH, HALB },
                                             { ZWANZIG, VOR },
                                             { VIERTEL, VOR },
                                             { ZEHN2, VOR },
                                             { FUENF2, VOR } };
    const std::vector<uint8_t> HOURS[13] { { EIN },   { EINS },  { ZWEI },   { DREI }, { VIER },
                                           { FUENF }, { SECHS }, { SIEBEN }, { ACHT }, { NEUN },
                                           { ZEHN },  { ELF },   { ZWOELF } };

} // namespace wordclock::led::definitions
