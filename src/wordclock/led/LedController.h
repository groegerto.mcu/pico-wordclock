#pragma once

#include <wordclock/Config.h>
#include <vector>

#include <picopp/led/LedController.h>
#include <picopp/led/PowerManagedLEDDriver.h>
#include <picopp/time/Datetime.h>

namespace wordclock::led
{

    class LedController
    {
      public:
        LedController(const Config& config);

        void run(picopp::Datetime time, uint8_t brightness);

        void turnOff();

        void showTestPattern();

      private:
        const static std::vector<uint8_t> IT_IS;
        const static std::vector<uint8_t> O_CLOCK;
        const static std::vector<uint8_t> MINUTES[12];
        const static std::vector<uint8_t> HOURS[13];
        static constexpr uint NUM_LEDS = 110;

        void applyTime(picopp::Datetime time);
        void resetLeds(picopp::CRGBW color);
        void applyLeds(const std::vector<uint8_t>& indices, const picopp::CRGBW& color);

      private:
        const Config& config;
        picopp::CRGBW leds[NUM_LEDS];
        picopp::LedDriver<picopp::CRGBW, picopp::WS2812Controller, picopp::GRBW> controller;
        picopp::PowerManagedLEDDriver<decltype(controller)> powerManager;

        // cache currently displayed values, so we can update only on changes
        int8_t currentMin = -1;
        uint8_t currentBrightness = 255;
    };

} // namespace wordclock::led
