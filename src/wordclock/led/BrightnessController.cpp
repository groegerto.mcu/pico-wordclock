#include "BrightnessController.h"

#include <picopp/util/math8.h>

namespace wordclock::led
{

    using namespace picopp::literals;

    BrightnessController::BrightnessController(const Config& config) :
      config(config), sensor(wordclock::conf::BRIGHTNESS_SENSOR_PIN)
    {
    }

    uint8_t
    BrightnessController::run()
    {
        if (config.automaticBrightness) {
            // for automatic brightness use user defined brightness as post automatic scaling
            return picopp::scale8(automaticBrightness(), config.brightness);
        } else {
            return config.brightness;
        }
    }

    uint8_t
    BrightnessController::automaticBrightness()
    {
        if (animation) {
            return animate(picopp::Timestamp::now());
        } else {
            uint8_t ambient = getAmbientValue();
            // hysteresis filter so brightness does not change rapidly
            if (std::abs(ambient - currentAmbient) >= conf::BRIGHTNESS_HYSTERESIS) {
                currentAmbient = ambient;
                uint8_t oldBrightness = targetBrightness;
                targetBrightness = calculateBrightness(currentAmbient);
                animation = { .start = picopp::Timestamp::now(),
                              .duration = conf::BRIGHTNESS_ANIMATION_DURATION,
                              .startBrightness = oldBrightness,
                              .endBrightness = targetBrightness };
                return oldBrightness;
            }
        }
        return targetBrightness;
    }

    uint8_t
    BrightnessController::getAmbientValue()
    {
        return sensor.read() >> 4;
    }

    uint8_t
    BrightnessController::calculateBrightness(uint8_t ambient)
    {
        return LOOKUP[ambient];
    }

    float
    BrightnessController::smoothstep(float edge0, float edge1, float x)
    {
        // smooth transition, see https://en.wikipedia.org/wiki/Smoothstep
        if (x < edge0) return 0;
        if (x >= edge1) return 1;

        // Scale/bias into [0..1] range
        x = (x - edge0) / (edge1 - edge0);

        return x * x * (3 - 2 * x);
    }


    uint8_t
    BrightnessController::animate(picopp::Timestamp time)
    {
        if ((time - animation->start) >= animation->duration) {
            targetBrightness = animation->endBrightness;
            animation.reset();
            return targetBrightness;
        } else {
            float frac = smoothstep(0, animation->duration.s_f(), (time - animation->start).s_f());
            return animation->startBrightness
                 + frac * (animation->endBrightness - animation->startBrightness);
        }
    }


} // namespace wordclock::led
