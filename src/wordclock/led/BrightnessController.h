#pragma once

#include <gcem.hpp>
#include <wordclock/Config.h>
#include <array>
#include <cmath>
#include <optional>

#include <picopp/hardware/ADC.h>
#include <picopp/time/Timestamp.h>


namespace wordclock::led
{

    namespace
    {
        template <typename T, uint Size>
        struct LookupTable {
            static_assert(std::is_integral_v<T>, "The lookup table only works for integral types!");
            static constexpr T DIV = (1 << (sizeof(T) * 8)) / Size;
            static constexpr uint SIZE = Size;

            constexpr LookupTable() : values()
            {
                for (uint i = 0; i < Size; i++) {
                    float frac = i / conf::BRIGHTNESS_MAX_OUTPUT / (float) (Size - 1);
                    float lux = conf::BRIGHTNESS_SENSOR_MIN_BRIGHTNESS
                              + frac
                                  * (conf::BRIGHTNESS_SENSOR_MAX_BRIGHTNESS
                                     - conf::BRIGHTNESS_SENSOR_MIN_BRIGHTNESS);
                    if (lux < 1254) {
                        float percent = 9.9323 * gcem::log(lux) + 27.059;
                        values[i] = percent * 255 / 100;
                    } else {
                        values[i] = 100;
                    }
                }
            }
            T
            operator[](T i) const
            {
                return values[i / DIV];
            }
            T values[Size];
        };
    } // namespace


    class BrightnessController
    {
      public:
        BrightnessController(const Config& config);

        uint8_t run();

      private:
        uint8_t automaticBrightness();
        uint8_t getAmbientValue();
        static uint8_t calculateBrightness(uint8_t ambient);

        struct animation {
            picopp::Timestamp start;
            picopp::Duration duration;
            uint8_t startBrightness;
            uint8_t endBrightness;
        };
        float smoothstep(float edge0, float edge1, float x);
        uint8_t animate(picopp::Timestamp time);

      public:
        static constexpr auto LOOKUP = LookupTable<uint8_t, 64>();

      private:
        const Config& config;
        picopp::ADC sensor;
        uint8_t currentAmbient;
        uint8_t targetBrightness;
        std::optional<animation> animation;
    };

} // namespace wordclock::led
