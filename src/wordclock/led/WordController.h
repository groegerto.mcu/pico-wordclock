#pragma once

#include <inttypes.h>
#include <vector>

namespace wordclock
{
    enum DisplayMode : uint8_t {
        // always show "Es ist"
        ALWAYS = 0,
        // only show "Es ist" on full and half hours
        FULL_HALF = 1,
        // never show "Es ist"
        NEVER = 2
    };

    namespace led
    {

        class WordController
        {
          public:
            static std::vector<uint8_t> getTimeWords(
              int8_t hour, int8_t minute, int8_t roundingOffset, DisplayMode mode, bool showOClock);
        };

    } // namespace led

} // namespace wordclock
