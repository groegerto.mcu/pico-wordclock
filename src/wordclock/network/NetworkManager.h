#pragma once

#include <wordclock/Config.h>
#include <optional>

#include <picopp/time/Timestamp.h>
#include <picopp/wifi/DHCPServer.h>
#include <picopp/wifi/DNSServer.h>
#include <picopp/wifi/Wifi.h>

namespace wordclock::network
{

    class NetworkManager
    {
      public:
        NetworkManager(const Config& config);

        NetworkManager(const NetworkManager&) = delete;
        NetworkManager& operator=(const NetworkManager&) = delete;

        bool isInitialized() const;
        bool isUp() const;
        bool hasInternet() const;

        void attemptConnection();

        void run();


      private:
        enum State {
            ERROR,
            NONE,
            CONNECTING,
            CONNECTED,
            ACCESS_POINT,
        };

        struct AP_Server {
            picopp::DHCPServer dhcp;
        };


      private:
        const Config& config;
        picopp::Wifi wifi;
        picopp::MDNSResponder mDNS;
        State state;
        picopp::Timestamp connectionTimeout {}; // time at which current connection attemp times out
        uint connectionAttempts = 0;            // number of times a connection was attempted
        std::optional<AP_Server> apServer = std::nullopt;
    };

} // namespace wordclock::network
