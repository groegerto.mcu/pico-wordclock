#include "Webserver.h"

#include <picopp/hardware/Watchdog.h>

namespace wordclock::network
{

    using namespace picopp::literals;

    Webserver::Webserver(FlashManager& flash,
                         NetworkManager& manager,
                         const time::TimeManager& time,
                         const std::function<void(int)>& overrideHandler) :
      flash(flash), server(), netManager(manager), time(time)
    {
        server.registerSSIHandler(
          "date", [this](const std::string& tag) { return this->time.getDisplayTime().format(); });

        server.registerSSIHandler("reboot", [](const std::string& tag) {
            return conf::RESTART_CAUSED_BY_WATCHDOG ? "true" : "false";
        });

        server.registerCgiHandler(
          "/reconnect", [this](const std::string& uri, int numParams, char* key[], char* value[]) {
              this->netManager.attemptConnection();
              return "/ok.html";
          });

        server.registerCgiHandler(
          "/reboot", [](const std::string& uri, int numParams, char* key[], char* value[]) {
              picopp::Watchdog::reboot(1_ms);
              while (1) {}
              return "";
          });

        server.registerCgiHandler(
          "/override",
          [overrideHandler](const std::string& uri, int numParams, char* key[], char* value[]) {
              if (numParams != 1 || std::strcmp(key[0], "dir") != 0) {
                  return "---"; // this will default to 404
              }
              int val = std::atoi(value[0]);
              overrideHandler(val);
              return "/ok.html";
          });

        server.registerCustomFileHandler("/config.json",
                                         [this](const std::string& uri, uint8_t& flags) {
                                             return this->flash.getConfig().generateJson();
                                         });

        server.registerPostHandler(
          "/save-configuration.post",
          [this](const std::string& uri, const std::string& header, std::string& data) {
              if (this->flash.getConfig().fromJson(data.data())) {
                  return "----"; // this will default to 404
              }
              this->flash.storeToFlash();
              return "/ok.html";
          });
    }

} // namespace wordclock::network
