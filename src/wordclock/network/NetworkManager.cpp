#include "NetworkManager.h"

#include <picopp/time/Duration.h>

namespace wordclock::network
{

    const static ip_addr_t ROUTER_IP = picopp::NetUtils::IP(192, 168, 4, 1);
    const static ip_addr_t ROUTER_MASK = picopp::NetUtils::IP(255, 255, 255, 0);

    using namespace picopp::literals;

    NetworkManager::NetworkManager(const Config& config) :
      config(config), wifi(), mDNS(), state(NONE)
    {
        if (wifi.init(picopp::Wifi::GERMANY)) {
            printf("[Wordclock] Error! Cannot initialize Wifi\n");
            state = ERROR;
            return;
        }
        if (mDNS.init()) {
            printf("[Wordclock] Error! Cannot initialize mDNS responder\n");
            state = ERROR;
            return;
        }
    }

    bool
    NetworkManager::isInitialized() const
    {
        return state != ERROR;
    }

    bool
    NetworkManager::isUp() const
    {
        return state == CONNECTED || state == ACCESS_POINT;
    }

    bool
    NetworkManager::hasInternet() const
    {
        return state == CONNECTED;
    }

    void
    NetworkManager::attemptConnection()
    {
        if (state == ACCESS_POINT) {
            apServer.reset();
        }
        // set ip address to zero, so new connection HAS to be made
        cyw43_state.netif[CYW43_ITF_STA].ip_addr.addr = 0;
        state = NONE;
    }

    void
    NetworkManager::run()
    {
        picopp::Wifi::poll();

        switch (state) {
            case NONE:
                if (config.ssid.length() == 0) {
                    state = ACCESS_POINT;
                } else {
                    wifi.enableStationMode();
                    connectionTimeout.setSinceBoot(0);
                    connectionAttempts = 0;
                    state = CONNECTING;
                }
                return;

            case CONNECTING:
                if (wifi.linkStatus() == picopp::Wifi::UP) {
                    state = CONNECTED;
                    printf("[Wordclock] Link to '%s' is UP (%s)!\n",
                           config.ssid.str(),
                           picopp::NetUtils::formatIpAddress(wifi.getIpAddress()).c_str());
                    mDNS.clearRecord();
                    mDNS.registerARecord(CYW43_HOST_NAME ".local", wifi.getIpAddress());
                    return;
                }

                // last connection attempt timed out
                if (connectionTimeout.reached()) {
                    // did we have an attempt which failed, or is this the first
                    if (connectionTimeout != picopp::Timestamp({ 0 })) {
                        printf("[Wordclock] Connection attemp to '%s' failed\n", config.ssid.str());
                        connectionAttempts++;
                    }

                    if (connectionAttempts >= 5) {
                        state = ACCESS_POINT;
                        printf("[Wordclock] Max connection attempts to '%s' reached!\n",
                               config.ssid.str());
                        return;
                    }

                    if (wifi.connectAsync(config.ssid, config.passwd)) {
                        // wait for small timeout, if async connect immediately fails
                        connectionTimeout = picopp::Timestamp::in(5_s);
                    } else {
                        connectionTimeout = picopp::Timestamp::in(20_s);
                    }
                }

                return;
            case CONNECTED:
                if (wifi.linkStatus() != picopp::Wifi::UP) {
                    printf("[Wordclock] Link to '%s' is DOWN\n", config.ssid.str());
                    state = NONE;
                }
                return;

            case ACCESS_POINT:
                if (!apServer) {
                    printf("[Wordclock] Setting up AP at SSID '%s'!\n", wordclock::conf::AP_SSID);
                    wifi.enableAPMode(wordclock::conf::AP_SSID,
                                      wordclock::conf::AP_PASSWD,
                                      picopp::Wifi::WPA2_AES_PSK);
                    apServer.emplace();

                    if (apServer->dhcp.init(ROUTER_IP, ROUTER_MASK, ROUTER_IP)) {
                        printf("[Wordclock] Setting up dhcp server failed\n");
                    }
                    mDNS.clearRecord();
                    mDNS.registerARecord(CYW43_HOST_NAME ".local", ROUTER_IP);
                }
                return;

            default: return;
        }
    }

} // namespace wordclock::network
