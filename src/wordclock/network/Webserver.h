#pragma once

#include <wordclock/FlashManager.h>
#include <wordclock/network/NetworkManager.h>
#include <wordclock/time/TimeManager.h>

#include <picopp/wifi/http/HTTPServer.h>

namespace wordclock::network
{

    class Webserver
    {
      public:
        Webserver(FlashManager& flash,
                  NetworkManager& manager,
                  const time::TimeManager& timge,
                  const std::function<void(int)>& overrideHandler);

      private:
        FlashManager& flash;
        picopp::HTTPServer server;
        NetworkManager& netManager;
        const time::TimeManager& time;
    };

} // namespace wordclock::network
