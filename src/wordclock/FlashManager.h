#pragma once

#include <wordclock/Config.h>

#include <picopp/flash/Flash.hpp>

namespace wordclock
{

    class FlashManager
    {
      public:
        FlashManager();

        Config& getConfig();
        void storeToFlash();

      private:
        static constexpr uint16_t MAGIC_NUMBER = 0x2501;
        struct FlashContent {
            // magic number to check if content in flash is valid
            uint16_t magic_number;
            Config config;
        };

        Config loadDefault();

      private:
        picopp::Flash<FlashContent, (1024 + 512) * 1024> flash;
    };

} // namespace wordclock
