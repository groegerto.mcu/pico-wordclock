# This is a copy of <LIB_PICOPP_PATH>/external/lib_picopp_import.cmake

# This can be dropped into an external project to help locate lib_pico++
# It should be include()ed prior to project()

if (DEFINED ENV{LIB_PICOPP_PATH} AND (NOT LIB_PICOPP_PATH))
    set(LIB_PICOPP_PATH $ENV{LIB_PICOPP_PATH})
    message("Using LIB_PICOPP_PATH from environment ('${LIB_PICOPP_PATH}')")
endif ()

if (NOT LIB_PICOPP_PATH)
    message(FATAL_ERROR
        "LibPico++ location was not specified. Please set LIB_PICOPP_PATH."
    )
endif ()

set(LIB_PICOPP_PATH "${LIB_PICOPP_PATH}" CACHE PATH "Path to the LibPico++")

get_filename_component(LIB_PICOPP_PATH "${LIB_PICOPP_PATH}" REALPATH BASE_DIR "${CMAKE_BINARY_DIR}")
if (NOT EXISTS ${LIB_PICOPP_PATH})
    message(FATAL_ERROR "Directory '${LIB_PICOPP_PATH}' not found")
endif ()

set(LIB_PICOPP_PATH ${LIB_PICOPP_PATH} CACHE PATH "Path to the LibPico++" FORCE)

add_subdirectory(${LIB_PICOPP_PATH} lib_picopp)
