#!/usr/bin/env bash
#
# generate_server_file_system.sh
#

FSDIR=server-fs
FS_FILE=http_config/wordclock_fsdata.c

if [ ! -f makefsdata ]; then
  $LIB_PICOPP_PATH/src/wifi/regen-fsdata.sh http_config/makefsdata .
fi

makefsdata ${FSDIR} -f:${FS_FILE}
