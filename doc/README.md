# Construction

[[_TOC_]]

The wordclock was mainly build from wood with an acrylic glass front plate.

The plan is available at [Frontplate/Frontplate.pdf](Frontplate/FrontPlate.pdf)

## Frame

<img src="Images/montage/frame_montage.jpg" alt="frame" />

---

The total dimensions for my clock are 350x350x43mm (WxLxH)
with the words inside a 250x250mm square.

The frame is built from beech wood and glued together.
It has a 44x44mm post in each corner to provide additional support.
The back plate is made from 4mm poplar plywood.
The outside and back of the frame is painted in black.

The power cable is attached on the back with a little box which is recessed into the body.
This means the back and the bottom are completely flat.
So it can be either hung from the wall or stood up without problem.

The grid is also made from 4mm poplar plywood and has foam at the top to prevent light bleed.
In the beginning there was still a lot of light bleeding through the corners and bottom of the plywood grid which was resolved with black acrylic grout.
I applied the grout to all edges inside the boxes of the grid and to the bottom edges.
To be able to detach the grid from the back plate I put cling film over the back plate before pressing the grid down onto it.
This meant I could still detach the grid from the back with no problem.
This worked very well and eliminated the light bleeding problem completely.

This image is a comparison of the light bleeding.
On the left is the grid before applying grout where there is a lot of light bleeding visible adjacent to the illuminated boxes.
On the right is the finished block with grout applied in a dark room. There is no more light bleeding visible.
<img src="Images/montage/frame_light_bleed.jpg" alt="frame with grid"/>

## LEDs

As LEDs I have chosen an RGBW WS2812 compatible LED strip.
This means I only need a single pin to control all LEDs.

As the spacing of the strip does not match my desired dimensions I cut them after each LED and soldered them together.

The power is provided for each row from the side to prevent a voltage drop through the whole strip.


## Front plate

<img src="Images/front_plate.jpg" alt="front plate" width="50%"/>

The front plate is attached to the frame with a big thumb screw in each corner. The screws were painted black to match the aesthetics of the clock.

The front plate is cut from black cardboard.
I designed it myself and had it cut at [formulor.de](https://www.formulor.de/).
To evenly light the letters there is a diffusion layer under the cardboard.

On top is a layer of 4mm acrylic glass.
I broke the glass to size by first scoring the glass where I wanted the cut to be and then breaking it off.
This left the edges quite rough which I sanded smooth with a chamfer.
The four wholes for the thumb screws were simply drilled with a metal drill.

## Electronics

The clock is controlled by a Raspberry Pico W.
The firmware can be found under [src](/src/).

The led strip required 5V as input voltage which the Pico also accepts so 5V was chosen as the power supply.
A 1000uF capacitor was attached directly to the power input as recommended by Neopixel for their LED strips.
The Pico only has 3.3V outputs which do not work with the 5V led strips so a 3.3V->5V level shifter was required.
I used a 74HCT32 OR gate which works with 3.3V inputs and outputs at 5V.

I also wanted to use an ambient light sensor which I attached to the Picos internal ADC.

The PCB and schematics can be found under [doc/PCB](/doc/PCB/).

<img src="Images/montage/pcb_montage.jpg" alt="front plate" />

&nbsp;

# Finished product

<img src="Images/montage/finished_both.jpg" alt="finished wordclock"/>

<img src="Images/finished_wall.jpg" alt="finished wordclock"/>
