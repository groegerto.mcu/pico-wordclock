# Wordclock

<img src="doc/Images/montage/finished_both.jpg" alt="finished wordclock"/>

## Planning

The design is inspired by [QlockTwo](https://qlocktwo.com/de/) and several other DIY projects I found on the internet:

- https://www.elektronika.ba/841/word-clock/
- https://www.instructables.com/Wordclock/ and https://github.com/wouterdevinck/wordclock
- https://github.com/mrrioes/WordclockV3
- https://www.mikrocontroller.net/articles/WordClock_mit_WS2812
- https://www.mikrocontroller.net/articles/Word_Clock#Variante_3:_160x160mm_Singleboard
- https://marcosprojects.wordpress.com/2018/12/24/esp8266-wifi-wordclock/ and https://github.com/marcow1601/Wordclock
- https://github.com/panbachi/wordclock
- https://www.instructables.com/Word-Clock-DIY/

---

My clock should have the following design:

- 10x11 Grid (110 LEDs total)
- Using a Raspberry Pi Pico W
- initial setup via WiFi access point
- integrated webserver for some configurations
- Set time through NTP
- Ambient light sensor to automatically adjust brightness


### Front plate design

As I am  german I want a german front plate design, though other languages are easily accesible on the internet.
I want to use what is described as "Rhein-Ruhr-Modus" on [mikrocontroller.net](https://www.mikrocontroller.net/articles/Word_Clock#Variante_3:_160x160mm_Singleboard).

That means the time is displayed as follows:
```
xx:00 Uhr: Es ist x Uhr
xx:05 Uhr: Es ist fünf nach x
xx:10 Uhr: Es ist zehn nach x
xx:15 Uhr: Es ist viertel nach x
xx:20 Uhr: Es ist zwanzig nach x
xx:25 Uhr: Es ist fünf vor halb x+1
xx:30 Uhr: Es ist halb x+1
xx:35 Uhr: Es ist fünf nach halb x+1
xx:40 Uhr: Es ist zwanzig vor x+1
xx:45 Uhr: Es ist viertel vor x+1
xx:50 Uhr: Es ist zehn vor x+1
xx:55 Uhr: Es ist fünf vor x+1
```

---

&nbsp;

More detailed information is available in [doc/](doc/) and [src/](src/).

<img src="doc/Images/finished_wall.jpg" alt="finished wordclock"/>
